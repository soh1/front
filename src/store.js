import _ from "underscore"

export default Vuex => {
  const store = new Vuex.Store({
    state: {
      app: "",
    },
    actions: {
      setApp({commit, state}, app) {
        commit('update', {app})
      }
    },
    mutations: {
      update(state, payload) {
        const keys = _.keys(payload)
        _.each(keys, key => {
          state[key] = payload[key]
        })
      }
    }
  })

  return store
}
