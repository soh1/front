import Root from "@/page/Root"
import Playground from "@/page/Playground"

export default VueRouter => {
  const routes = [
    {
      path: "/",
      component: Root,
      name: "Root",
    },
    {
      path: "/playground",
      component: Playground,
      name: "Playground",
    },
  ]

  return new VueRouter({
    routes,
    mode: "history"
  })
}
