import 'es6-promise/auto'

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import routerFactory from "@/router"
import storeFactory from "@/store"

Vue.use(VueRouter)
Vue.use(Vuex)

import App from '@/App.vue'

Vue.config.productionTip = false

const router = routerFactory(VueRouter)
const store = storeFactory(Vuex)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
